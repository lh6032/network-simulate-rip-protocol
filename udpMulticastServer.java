package P2;

/**
 * provided by professor: Sam Fryer
 */

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class udpMulticastServer extends Thread {
    public int port = 63001;
    public String broadcastIPAddress;
    private byte[] receiveData = new byte[1024];
    public static Router[] routerArr;


    public udpMulticastServer(int port, String broadcastIPAddress){
        this.port = port;
        this.broadcastIPAddress = broadcastIPAddress;
    }

    public void receiveData(){
        try {
            MulticastSocket multicastSocket = new MulticastSocket(port);
            System.out.println("Listening on port: " + multicastSocket.getLocalPort());
            System.out.println();
            InetAddress group = InetAddress.getByName("230.0.0.0");
            multicastSocket.joinGroup(group);

            for(;;){
                DatagramPacket datagramPacket = new DatagramPacket(receiveData, receiveData.length);
                multicastSocket.receive(datagramPacket);
                receiveData = Router.result.getBytes();
                datagramPacket = new DatagramPacket(receiveData, receiveData.length, InetAddress.getByName(broadcastIPAddress), port);
                DatagramSocket datagramSocket = new DatagramSocket();
                datagramSocket.send(datagramPacket);

                Scanner sc = new Scanner(System.in);
                if(sc.next().isBlank()){
                    break;
                }
                sc.close();
            }
            multicastSocket.leaveGroup(group);
            multicastSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void run(){
        receiveData();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
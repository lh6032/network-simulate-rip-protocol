package P2;

/**
 * This program simulates RIP protocol to test sending and updating routing tables.
 * Each node can be sender and receiver which are implemented by multi-threads.
 *
 * Author: Sophie Hou (lh6032)
 */

import java.net.InetAddress;
import java.util.*;


public class Router extends Thread {

    public  static Router[] routerArr; // to store all nodes.
    public static String result = "";
    public static int routerNum = 0;
    public static int blockNum = 0;
    public static int channelNum = 0;

    public  ArrayList<int[]> routingTable = null; // to store all nodes' routing tables
    public int[] cost;
    public int[] nextHop;
    public int routerID;

    public String sensorAddress = "";
    public String routerAddress = "";

    public Router() {};

    public Router(int routerID, String sensorAddress, String routerAddress) {
        this.routerID = routerID;
        this.sensorAddress = sensorAddress;
        this.routerAddress = routerAddress;

        routingTable = new ArrayList<>(routerNum);  // routing table
        initialRoutingTable();   // test, check initial routing table value

        cost = new int[routerNum]; // only store each node's updated cost
        initialCostTable();
        System.out.println("CHECKING TABLE_INITIAL -- Router "+ routerID + ":\t" + Arrays.toString(cost));

        nextHop = new int[routerNum]; // store nextHop's node id, i.e. 0,1,2..., which will attach to router address
        for (int i = 0; i < routerNum; i++) {
            nextHop[i] = i;   // initial next hop as its own router
        }
    }


    /**
     * Initial each node's cost to others as infinity, and to itself as 0
     */
    private void initialCostTable() {
        for (int i = 0; i < routerNum; i++) {
            cost[i] = Integer.MAX_VALUE;   // set cost to others as infinity
            if (i == routerID) {
                cost[i] = 0;             // set cost to itself as 0
            }
        }
    }


    /**
     * Initial each node's routing table as infinity, which contains all nodes cost information
     */
    private void initialRoutingTable() {
        for (int i = 0; i < routerNum; i++) {
            int[] eachRow = new int[routerNum];
            Arrays.fill(eachRow, Integer.MAX_VALUE); // initial each cost as infinity
            routingTable.add(i, eachRow);
        }
    }


    /**
     * print checking table to compare with routing table
     */
    private void printCheckingTable() {
        System.out.println("==================================================");
        for (int[] ints : routingTable) {
            System.out.print("CHECKING TABLE:  ");
            System.out.println(Arrays.toString(ints));
        }
    }

    /**
     * print formatted routing table, which contains destination address, next hop address and cost.
     */
    public  void printRoutingTable() throws InterruptedException {
        String result = "";
        System.out.println("ROUTING TABLE OF ROUTER " + routerID);
        System.out.println("Address\t\t\t\tNext Hop\t\tCost/Hop Times");
        System.out.println("==================================================");
        for (int i = 0; i < routerNum; i++) {
            String nextHopAddress = routerArr[nextHop[i]].routerAddress;
            int minCostInt =  routerArr[i].routingTable.get(routerID)[i];
            String minCostStr = "";
            if(minCostInt == Integer.MAX_VALUE){
                minCostStr = "Disconnected";    // set nteger.MAX_VALUE as "disconnected"
            }else{
                minCostStr = minCostInt + "";
            }
            result += routerArr[i].sensorAddress + "\t\t\t" + nextHopAddress + "\t\t\t" + minCostStr +"\n";

        }
        System.out.println(result);
        sleep(100);
        System.out.println();
    }


    /**
     * Update routing table. Create multi- threads to implement.
     * Each node has its own thread to run by using distance vector algorithm.
     * @throws InterruptedException
     */
    public void updateRoutingTable() throws InterruptedException {  //each router update its routing table by thread
        System.out.println("Before Updating The Checking Table of Router(id): " + routerID);
        printCheckingTable();
        System.out.println("--------------------------------------------------");
        System.out.println("After Updating The Routing Table of Router(id): " + routerID);
        for (int i = 0; i < routerNum; i++) {
            UpdateRoutingTable[] updateRoutingTables = new UpdateRoutingTable[routerNum];
            (updateRoutingTables[i] = new UpdateRoutingTable(routerArr, routingTable, routerNum, nextHop)).start();

        }

        printRoutingTable();
        System.out.println();
        Thread.sleep(500);
    }


    /**
     *  Send the temporary stored cost to other nodes by running multi-threads.
     */
    public void sendTable() {
        for (int i = 0; i < routerNum; i++) {
            SendRoutingTable[] sendRoutingTablesArr = new SendRoutingTable[routerNum];
            (sendRoutingTablesArr[i] = new SendRoutingTable(routerArr, cost, routerID, routerNum)).start();
        }
    }


    /**
     * Read blocked number from user input, and block randomly selected channels, set the cost as infinity.
     * Connect other channels , set cost between connected nodes as 1
     */
    public static void connectChannel() {
        System.out.println("\n");
        System.out.println("Connect Channel...");
        int rand_int1 = 0, rand_int2 = 0;

        for (int index = 0; index < routerNum; index++) {
            for (int index2 = 0; index2 < routerNum; index2++) {
                if (index == index2) {
                    routerArr[index].cost[index2] = 0;
                } else {
                    routerArr[index].cost[index2] = 1;
                }
            }
        }
        // get blocked nodes
        for (int i = 0; i < blockNum; i++) {
            Random random = new Random();
            rand_int1 = random.nextInt(routerNum); //0...n randomly choose router1
            rand_int2 = random.nextInt(routerNum); //0...n randomly choose router2

            if (rand_int2 == rand_int1 && rand_int2 != 0) { // two nodes should be different.
                rand_int2 = rand_int1 - 1;
            }
            if (rand_int2 == rand_int1 && rand_int1 != (routerNum - 1)) {
                rand_int2 = rand_int1 + 1;
            }

            System.out.println("Blocked Channel (" + (i + 1) + ") , Router (ID): " + (rand_int1) + " --> " + (rand_int2));

            routerArr[rand_int1].cost[rand_int2] = Integer.MAX_VALUE;   // set blocked channel as infinity

        }
        System.out.println("--------------------------------------------------");

        //print each router to others' cost
        for (int i = 0; i < routerNum; i++) {
            System.out.println(("CHECKING TABLE -- Node " + routerArr[i].routerID) + ": " + Arrays.toString(routerArr[i].cost));
        }
        System.out.println();
    }


    /**
     * Get the maxium channels that can be blocked
     * @param routerNum
     * @return
     */
    private static int factorial(int routerNum) {
        return (routerNum == 1 || routerNum == 0 ? 1 : routerNum * factorial(routerNum - 1));
    }


    /**
     * User input nodes' number and blocked channels' number
     * Call sendTable() and updateRoutingTable() to implement multi-threads tasks
     * Call connectChannel() to create a new topology.
     * @throws InterruptedException
     */
    public static void makeNetTopology() throws InterruptedException {
        System.out.println("Input the number of nodes ( n > 0 ): ");
        Scanner scRouter = new Scanner(System.in);
        if (scRouter.hasNext()) {
            routerNum = scRouter.nextInt();  // get nodes' number
        }

        System.out.println("Input the number of channels you want to block  : ");
        Scanner scChannel = new Scanner(System.in);    // get blocked channel number

        if (scRouter.hasNext()) {
            blockNum = scRouter.nextInt();
        }
        int maxNumOfChannels = factorial(routerNum);  // max num of channels

        if (blockNum > maxNumOfChannels) {
            blockNum = maxNumOfChannels;      // block channels' number cannot exceed maximum channels
        }
        channelNum = maxNumOfChannels - blockNum;   // get connected channel number


        // create multi threads to send and update table
        // call sendTable() and updateRoutingTable() to implement multi-threads tasks
        routerArr = new Router[routerNum];  // create Node's instances
        for (int i = 0; i < routerNum; i++) {
            routerArr[i] = new Router(i, "10.0." + (i + 1) + ".0/24", "172.18.0." + (21 + i));
            routerArr[i].start();

            sleep(100);
        }
        connectChannel();     // connect channel

        if(scChannel.nextLine().isBlank()){  // 'return' to exit the program
            System.exit(0);
        }

        scRouter.close();
        scChannel.close();
    }


    /**
     * Each node run sending table and updating table independently.
     * Set 10 seconds timer to monitor updating.
     */
    public void run() {
        long start = new Date().getTime();// timer starts timing

        for(;;) {

            try {
                Thread.sleep(5000);
                sendTable();     // create multi threads by calling SendRoutingTable.java
                updateRoutingTable();  // create multi threads by calling UpdateRoutingTable.java
                long offLine = new Date().getTime();

                if(offLine - start > 10 *1000) {   // timer ends timing
                    System.out.println("Node " + routerID + " : Time is up ! Offline.");
                    break;
                }
                Thread.currentThread().sleep(1);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * udpMulticastServer
     * @param address
     */
    public static void setServer(String address){
        // Starting Multicast Receiver
        System.out.println("Starting Multicast Receiver...");
        udpMulticastServer server = new udpMulticastServer(63001,address);
        server.start();
    }


    /**
     * udpMulticastClient
     * @param address
     */
    public static void setClient(String address){
        // Starting Multicast Sender
        System.out.println("Starting Multicast Sender...");
        udpMulticastClient client = new udpMulticastClient(63001,address);
        client.start();
    }


    public static void main(String[] args) {
        try
            {
                InetAddress localhost = InetAddress.getLocalHost();
                String address = (localhost.getHostAddress()).trim();
                System.out.println("Broadcasting from: "+address);

                setServer(address);
                sleep(100);

                makeNetTopology();  //initialize topology

                setClient(address);
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
    }
}

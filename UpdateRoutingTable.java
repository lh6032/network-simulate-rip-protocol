package P2;

/**
 * This program tests updating routing tables.
 * Each node can be sender and receiver which are implemented by multi-threads.
 *
 * Author: Sophie Hou (lh6032)
 */

import java.util.ArrayList;
import java.util.Arrays;

public class UpdateRoutingTable extends Thread {
        public Router[] routerArr;
        public static ArrayList<int[]> routingTable;
        public static int routerNum;
        public static int[] nextHop;


    /**
     * Constructor
     * @param routerArr
     * @param routingTable
     * @param routerNum
     * @param nextHop
     */
    public UpdateRoutingTable(Router[] routerArr, ArrayList<int[]> routingTable, int routerNum, int[] nextHop){
        this.routerArr = routerArr;
        UpdateRoutingTable.routingTable = routingTable;
        UpdateRoutingTable.routerNum = routerNum;
        UpdateRoutingTable.nextHop = nextHop;
    }


    /**
     * Call doTheworkUpdating()
     */
    public void run(){
         doTheworkUpdating();
    }


    /**
     * Implements Distance Vector Algorithm.
     * Compare costs to get the minimal cost.
     */
    public void doTheworkUpdating(){
        ArrayList<Boolean> visited;
        int count = 0;


        int[] countOneArr = new int[routerNum];  // count the number of connected nodes for each node

        for(int j = 0; j < routerNum; j++){
            count = 0;
            for (int l = 0; l < routerNum; l++) {
                if(routingTable.get(j)[l] == 1){
                    count++;
                }
            }
            countOneArr[j] = count;   //store occurrence of 1, which indicates connected channels.
        }


        for(int index1 = 0; index1 < routerNum; index1++){ // check each row (node) of the routing table

            visited = new ArrayList<>(routerNum); // store status of visited: true or false
            for (int i = 0; i < routerNum ; i++) {
                visited.add(i, false);  // set visited other nodes: false
            }
            visited.set(index1, true);     //  set visited the node itself: true

            int[] countOne = Arrays.copyOf(countOneArr, routerNum); // copy countOneArr

            while(visited.contains(false)) { // for each node, if has unvisited node, then keep checking

                for (int index2 = 0; index2 < routerNum; index2++) {
                    if ((routingTable.get(index1)[index2] < Integer.MAX_VALUE) && (routingTable.get(index1)[index2] > 0)
                            && (!visited.get(index2))){

                        for (int k = 0; k < routerNum; k++) {
                            if (routingTable.get(index2)[k] > 0 && routingTable.get(index2)[k] < Integer.MAX_VALUE) {
                                if (routingTable.get(index1)[k] > routingTable.get(index1)[index2] + routingTable.get(index2)[k]) {
                                    routerArr[index1].cost[k] = routingTable.get(index1)[index2] + routingTable.get(index2)[k]; // changed data only store inside each node' cost[]
                                    nextHop[k] = index2; // store next hop
                                }
                                countOne[index2]--;

                                if(countOne[index2] == 0){  // indicates checked all adjacent nodes
                                    visited.set(index2, true); // mark checked node as true
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

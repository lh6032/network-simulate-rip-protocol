package P2;

/**
 * This program tests sending routing tables.
 * Each node can be sender and receiver which are implemented by multi-threads.
 *
 * Author: Sophie Hou (lh6032)
 */

public class SendRoutingTable extends Thread {
    public Router[] routerArr;
    public int routerID;
    public int routerNum;
    public int[] cost;

    /**
     * constructor
     * @param routerArr
     * @param cost
     * @param routerID
     * @param routerNum
     */
    public SendRoutingTable(Router[] routerArr, int[] cost, int routerID, int routerNum){
        this.routerArr = routerArr;
        this.cost = cost;
        this.routerID = routerID;
        this.routerNum = routerNum;
    }

    /**
     * Call doTheworkSending().
     */
    public void run(){
        int index = 1;
        for(;;){
            doTheworkSending();
            index++;

            if(index == routerNum)
                break;
        }
    }

    /**
     * Updating each nodes's routing table.
     */
    private void doTheworkSending() {
        for (int i = 0; i < routerNum; i++) {
            routerArr[i].routingTable.set(routerID, cost);
        }
    }
}


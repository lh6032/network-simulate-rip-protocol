package P2;

/**
 * provided by professor: Sam Fryer
 */


import java.io.IOException;
import java.net.*;

public class udpMulticastClient extends Thread{
    public int port = 63001;
    public String broadcastIPAddress;
    public byte[] sendData = new byte[1024];

    public udpMulticastClient(int port, String broadcastIPAddress){
        this.port = port;
        this.broadcastIPAddress = broadcastIPAddress;
    }

    public void sendData() throws IOException {
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress group = InetAddress.getByName(broadcastIPAddress);
        sendData = Router.result.getBytes();


        DatagramPacket datagramPacket = new DatagramPacket(sendData, sendData.length, group, port);
        clientSocket.send(datagramPacket);
        clientSocket.close();
    }

    public void run(){
        for(;;){
            try{
                sendData();
                Thread.sleep(500);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }

}
